package org.gcube.common.core.publisher.is.legacy.stubs.fault;

import org.gcube.common.core.publisher.is.legacy.stubs.RegistryStub;

import jakarta.xml.ws.WebFault;

/**
 * Thrown by {@link RegistryStub#remove(String, String)} when the resource does not exist in the collector 
 */
@WebFault(name = "ResourceDoesNotExistFault")
public class ResourceDoesNotExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Creates an instance with a given message and an {@link AxisFaultInfo} payload
	 * @param message the message
	 * @param info the payload
	 */
	public ResourceDoesNotExistException(String message) {
		super(message);
	}
}
