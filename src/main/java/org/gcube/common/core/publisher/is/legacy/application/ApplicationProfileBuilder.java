package org.gcube.common.core.publisher.is.legacy.application;

import java.net.URI;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import jakarta.servlet.ServletRegistration;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.smartgears.configuration.ProxyAddress;
import org.gcube.smartgears.configuration.application.ApplicationConfiguration;
import org.gcube.smartgears.configuration.container.ContainerConfiguration;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationProfileBuilder {

	private static List<String> servletExcludes = Arrays.asList("default","jsp");

	private static final Logger log = LoggerFactory.getLogger(ApplicationProfileBuilder.class);
	
	private ApplicationContext context;

	public ApplicationProfileBuilder(ApplicationContext context) {
		this.context = context;
	}

	public GCoreEndpoint create() {
		
		GCoreEndpoint endpoint = new GCoreEndpoint();

		endpoint.setId(context.id());
		ApplicationConfiguration configuration = context.configuration();
		ContainerConfiguration container = context.container().configuration();


		endpoint.profile()
		.description(configuration.description())
		.serviceName(configuration.name())
		.serviceClass(configuration.group())
		.version(configuration.version())
		.serviceId(configuration.name() + configuration.group() + configuration.version())
		.ghnId(context.container().id());

		endpoint.profile().newDeploymentData()
		.activationTime(Calendar.getInstance())
		.status((context.lifecycle().state().remoteForm()));
		
		log.debug("publishing aplication profile with id {} for {} with status {}",context.id(), configuration.name(), context.lifecycle().state().remoteForm());
		
		endpoint.profile().endpoints().clear();
		
		String baseAddress;
		if (configuration.proxable() && container.proxy()!=null){
			ProxyAddress proxy = container.proxy();
			String protocol = proxy.getProtocol();
			String port = proxy.getPort()!=null?":"+proxy.getPort():"";

			baseAddress=String.format("%s://%s%s%s", protocol , proxy.getHostname(), port,context.application().getContextPath());
		} else {
			String protocol = container.protocol();
			int port = container.port();

			baseAddress=String.format("%s://%s:%d%s", protocol , container.hostname(), port,context.application().getContextPath());
		}
		
		for (ServletRegistration servlet : context.application().getServletRegistrations().values())
			if (!servletExcludes.contains(servlet.getName()))
				for (String mapping : servlet.getMappings()) {
					String address = baseAddress+(mapping.endsWith("*")?mapping.substring(0,mapping.length()-2):mapping);
					endpoint.profile().endpoints().add().nameAndAddress(servlet.getName(),URI.create(address));
				}

		return endpoint;
	}

}
