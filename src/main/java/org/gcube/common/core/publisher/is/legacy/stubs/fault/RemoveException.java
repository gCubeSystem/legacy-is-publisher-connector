package org.gcube.common.core.publisher.is.legacy.stubs.fault;

import org.gcube.common.core.publisher.is.legacy.stubs.RegistryStub;

import jakarta.xml.ws.WebFault;

/**
 * Thrown by {@link RegistryStub#remove(String, String)} when something is failed on remove 
 */
@WebFault(name = "RemoveFault")
public class RemoveException extends PublisherException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Creates an instance with a given message and an {@link AxisFaultInfo} payload
	 * @param message the message
	 * @param info the payload
	 */
	public RemoveException(String message) {
		super(message);
	}
}