package org.gcube.common.core.publisher.is.legacy;

public class RegistryNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8265959805049300612L;

	public RegistryNotFoundException() {
		super();
	}

	public RegistryNotFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public RegistryNotFoundException(String arg0) {
		super(arg0);
	}

	public RegistryNotFoundException(Throwable arg0) {
		super(arg0);
	}

	
	
	
}
