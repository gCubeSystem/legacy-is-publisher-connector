package org.gcube.common.core.publisher.is.legacy.stubs.fault;



import org.gcube.common.core.publisher.is.legacy.stubs.RegistryStub;

import jakarta.xml.ws.WebFault;

/**
 * Thrown by {@link RegistryStub#create(String, String)} when the resource is not accepted cause it doesn't satisfy a requirement 
 */
@WebFault(name = "ResourceNotAcceptedFault")
public class ResourceNotAcceptedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Creates an instance with a given message and an {@link AxisFaultInfo} payload
	 * @param message the message
	 * @param info the payload
	 */
	public ResourceNotAcceptedException(String message) {
		super(message);
	}
}
