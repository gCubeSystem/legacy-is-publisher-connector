package org.gcube.common.core.publisher.is.legacy.stubs;

import static org.gcube.common.core.publisher.is.legacy.stubs.RegistryConstants.portType;
import static org.gcube.common.core.publisher.is.legacy.stubs.RegistryConstants.target_namespace;

import org.gcube.common.core.publisher.is.legacy.stubs.fault.CreateException;
import org.gcube.common.core.publisher.is.legacy.stubs.fault.InvalidResourceException;
import org.gcube.common.core.publisher.is.legacy.stubs.fault.RemoveException;
import org.gcube.common.core.publisher.is.legacy.stubs.fault.ResourceDoesNotExistException;
import org.gcube.common.core.publisher.is.legacy.stubs.fault.ResourceNotAcceptedException;
import org.gcube.common.core.publisher.is.legacy.stubs.fault.UpdateException;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;

/**
 * A local interface to the resource discovery service.
 * 
 * 
 */
@WebService(name=portType,targetNamespace=target_namespace)
public interface RegistryStub {

	/**
	 * 
	 * @param profile the profile in xml
	 * @param type the type of the resource to store
	 * 
	 * @throws InvalidResourceException if the profile is not valid
	 * @throws ResourceNotAcceptedException if some filter is applied registry side
	 * @throws CreateException if something goes wrong on creation
	 */
	@WebMethod(operationName="create")
	@WebResult()
	void create(@WebParam(name="profile") String profile, @WebParam(name="type") String type )  throws InvalidResourceException,
																					ResourceNotAcceptedException, CreateException;
	/**
	 * 
	 * @param id the id of the resource to update
	 * @param type the type of the resource to update
	 * @param profile the profile in xml	 
	 *  
	 * @throws InvalidResourceException if the profile is not valid
	 * @throws ResourceNotAcceptedException if some filter is applied registry side
	 * @throws UpdateException if something goes wrong on update
	 */
	@WebMethod(operationName="update")
	@WebResult()
	void update(@WebParam(name="uniqueID") String id, @WebParam(name="type") String type, @WebParam(name="xmlProfile") String profile )  throws InvalidResourceException,
																					ResourceNotAcceptedException, UpdateException;
	/**
	 * 
	 * @param id the id of the resource to remove
	 * @param type the type of the resource to remove
	 *  
	 * @throws ResourceDoesNotExistException if the resource is not stored on the Collector
	 * @throws RemoveException if something goes wrong during deletion
	 */
	@WebMethod(operationName="remove")
	@WebResult()
	void remove(@WebParam(name="uniqueID") String id, @WebParam(name="type") String type)  throws ResourceDoesNotExistException, 
																								RemoveException;

	
}
