package org.gcube.common.core.publisher.is.legacy;

import java.util.Collections;
import java.util.Map;

import org.gcube.common.security.Owner;
import org.gcube.common.security.secrets.Secret;

public class ICSecret extends Secret{

	private String context;
		
	protected ICSecret(String context) {
		this.context = context;
	}

	@Override
	public Owner getOwner() {
		return new Owner("guest", Collections.emptyList(), false, false);
	}

	@Override
	public String getContext() {
		return context;
	}

	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		return Collections.emptyMap();
	}

	@Override
	public boolean isValid() {
		return true;
	}

	public boolean isExpired() {
		return false;
	}

	

	
}
