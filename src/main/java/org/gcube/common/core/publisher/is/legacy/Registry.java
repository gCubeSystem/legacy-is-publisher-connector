package org.gcube.common.core.publisher.is.legacy;

import static org.gcube.common.clients.stubs.jaxws.StubFactory.stubFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.net.URI;
import java.util.List;

import org.gcube.common.core.publisher.is.legacy.stubs.RegistryConstants;
import org.gcube.common.core.publisher.is.legacy.stubs.RegistryStub;
import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.api.ResultParser;
import org.gcube.resources.discovery.client.impl.DelegateClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Registry {
	
	private RegistryCache cache = new RegistryCache(10);
	private List<URI> endpoints;
	private static final Logger log = LoggerFactory.getLogger(Registry.class);
	
	public List<URI> getEndPoints(){
		String scope=SecretManagerProvider.get().getContext();

		endpoints=(List<URI>)cache.get(scope);
		if(endpoints==null){
			SimpleQuery query = queryFor(GCoreEndpoint.class);
			ResultParser<URI> uriParser =new ResultParser<URI>() {

				@Override
				public URI parse(String result) throws Exception {
					return new URI(result.replaceAll("\n", ""));
				}
				
			};
			DiscoveryClient<URI> client = new DelegateClient<URI>(uriParser, new ICClient());
			query.addCondition("$resource/Profile/ServiceClass/text() eq '"+RegistryConstants.service_class+"'")
				.addCondition("$resource/Profile/ServiceName/text() eq '"+RegistryConstants.service_name+"'")
				.setResult("$resource/Profile/AccessPoint/RunningInstanceInterfaces/Endpoint[string(@EntryName) eq '"+RegistryConstants.service_entrypoint+"']/string()");
			endpoints = client.submit(query);
			if (endpoints.size()==0){
				throw new IllegalArgumentException("No endpoint for registry found");
			}
			cache.put(scope, endpoints);
		}
		return endpoints;
	}
	
	public RegistryStub getStubs() throws RegistryNotFoundException{
		String context=SecretManagerProvider.get().getContext();
		URI endpoint=null;
		//use another method to cache epr
		endpoint = getEndPoints().get(0);
		log.debug("get stubs in context {} from endpoint {} ", context, endpoint);
		return stubFor(RegistryConstants.registry).at(endpoint);
	}
	
	public RegistryStub getStubs(URI endpoint) throws RegistryNotFoundException{
		String context=SecretManagerProvider.get().getContext();
		log.debug("get stubs in context {} from fixed endpoint {} ", context, endpoint);
		return stubFor(RegistryConstants.registry).at(endpoint);
	}

}
