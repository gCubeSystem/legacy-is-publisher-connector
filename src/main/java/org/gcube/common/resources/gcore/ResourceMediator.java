package org.gcube.common.resources.gcore;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResourceMediator {

	private static final Logger log = LoggerFactory.getLogger(ResourceMediator.class);
	
	public static void setId(Resource resource, String id) {
	     resource.setId(id);
	     log.debug(id+"new id resource: "+resource.id());
	}
	
	public static void setScope(Resource resource, String scope){
		resource.scopes().add(scope);
	}
	public static void removeScope(Resource resource, String scope){
		resource.scopes().remove(scope);
	}
	
	public static Resource cleanAllScopes(Resource resource){
		log.debug("removing scopes from resource: "+resource.id());
		List<String> toRemove= new ArrayList<String>();
		int count=0;
		for (String scope :resource.scopes()){
			System.out.println("found scope "+scope+" ");
			toRemove.add(scope);
			
		}
		for(String scope: toRemove){
			log.debug("removing scope "+scope+" ");
			resource.removeScope(scope);
			count++;
		}
		log.debug("removed "+count+" scopes");
		return resource;
	}
}
