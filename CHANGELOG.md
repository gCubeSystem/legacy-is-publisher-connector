This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Legacy IS Publisher Connector


## [v1.0.0] 

- First Release

